//
//  main.m
//  Dice
//
//  Created by Kornelius Kalnbach on 03.08.12.
//  Copyright (c) 2012 Kornelius Kalnbach. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DiceAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DiceAppDelegate class]));
    }
}
