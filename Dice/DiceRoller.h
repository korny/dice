//
//  DiceRoller.h
//  Dice
//
//  Created by Kornelius Kalnbach on 03.08.12.
//  Copyright (c) 2012 Kornelius Kalnbach. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiceRoller : NSObject

- (NSArray *)rollDiceWithNumberOfDice:(NSInteger)numberOfDice usingEdge:(BOOL)useEdge;

@end
