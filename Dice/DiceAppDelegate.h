//
//  DiceAppDelegate.h
//  Dice
//
//  Created by Kornelius Kalnbach on 03.08.12.
//  Copyright (c) 2012 Kornelius Kalnbach. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiceAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
