//
//  DiceViewController.m
//  Dice
//
//  Created by Kornelius Kalnbach on 03.08.12.
//  Copyright (c) 2012 Kornelius Kalnbach. All rights reserved.
//

#import "DiceViewController.h"
#import "DiceRoller.h"

@interface DiceViewController ()
@property (weak, nonatomic) IBOutlet UILabel *diceDisplay;
@property (weak, nonatomic) IBOutlet UISwitch *useEdge;
@property (weak, nonatomic) IBOutlet UIStepper *diceStepper;
@property (weak, nonatomic) IBOutlet UILabel *numberOfDices;

@property (strong, nonatomic) DiceRoller *diceRoller;
@end

@implementation DiceViewController

NSInteger numberOfDice = 1;

- (DiceRoller *)diceRoller {
    if (!_diceRoller) _diceRoller = [[DiceRoller alloc] init];
    return _diceRoller;
}


- (IBAction)throwOneDice:(UIButton *)sender {
    BOOL useEdge = self.useEdge.on;
    
    NSArray *dice = [self.diceRoller rollDiceWithNumberOfDice:numberOfDice usingEdge:useEdge];
    
    // Es gibt doch bestimmt eine elegantere Lösung, oder?
    self.diceDisplay.text = [dice componentsJoinedByString:@" "];
    if (numberOfDice <= 12) {
        self.diceDisplay.text = [self.diceDisplay.text stringByReplacingOccurrencesOfString:@"1" withString:@"⚀"];
        self.diceDisplay.text = [self.diceDisplay.text stringByReplacingOccurrencesOfString:@"2" withString:@"⚁"];
        self.diceDisplay.text = [self.diceDisplay.text stringByReplacingOccurrencesOfString:@"3" withString:@"⚂"];
        self.diceDisplay.text = [self.diceDisplay.text stringByReplacingOccurrencesOfString:@"4" withString:@"⚃"];
        self.diceDisplay.text = [self.diceDisplay.text stringByReplacingOccurrencesOfString:@"5" withString:@"⚄"];
        self.diceDisplay.text = [self.diceDisplay.text stringByReplacingOccurrencesOfString:@"6" withString:@"⚅"];
    }
    
    [self.useEdge setOn:NO animated:YES];
}

- (IBAction)diceStepperChanged {

    self.numberOfDices.text = [NSString stringWithFormat:@"%ld", (long)self.diceStepper.value];
    numberOfDice = self.diceStepper.value;
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

@end
