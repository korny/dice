//
//  DiceRoller.m
//  Dice
//
//  Created by Kornelius Kalnbach on 03.08.12.
//  Copyright (c) 2012 Kornelius Kalnbach. All rights reserved.
//

#import "DiceRoller.h"

@implementation DiceRoller

- (NSInteger)rollOneDie {
    float randomNumber = 1.0 * rand() / RAND_MAX;
    return floor(randomNumber * 6) + 1;
}

- (NSArray *)rollDiceWithNumberOfDice:(NSInteger)numberOfDice usingEdge:(BOOL)useEdge {
    NSMutableArray *dice = [NSMutableArray array];
    
    while (numberOfDice) {
        NSInteger die = [self rollOneDie];
        [dice addObject:[NSNumber numberWithInteger:die]];
        if (!useEdge || die < 6) numberOfDice--;
    }
    
    return [dice copy];
}
    
@end
